# Dengiki Bunko: Crossing Void - raw assets

This git contains the extracted assets of the game [Dengiki Bunko: Crossing Void](https://www.37games.com/dbcv) by 37GAMES.

The assets were copied from ``/Android/data/com.ujoy.dbcv/`` and extracted via [UnityPy](https://github.com/K0lb3/UnityPy).